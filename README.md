# Gitlab CI to EC2

## About This Repo

This Repository containes an example nodejs code and Dockerfile required to build docker image of this nodejs app. Then, ``` .gitlab-ci.yml ``` file is configured to build and push docker image to container registry in the gitlab.

## About CI/CD

CI/CD for this repositroy contains two stages:

- ### Build Image
 > In this stage, a Docker image will be built according to Dockerfile and pushed to gitlab registry with address registry.gitlab.com.

- ### Deploy in EC2
 > In this stage, Gitlab runner will try to ssh into ec2 instance with provided private key then pull the docker image from Gitlab registry and then finally deploy it using docker run command.

## Usage

> You don't need to do anything. Just push to the repo and pipeline will start trigger if your branch is **main** .

## Reference

This Repository is the code reference for [this blog post](https://blog.heinux-training.net/deploying-to-aws-ec2-from-gitlab-ci).
